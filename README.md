# tuto_htmx

## Lancer Django
- Allez à la racine du projet ( tuto_htmx/ )
- Créer un environnement virtuel avec  ``` python -m venv venv ```
- Activer l'environnement :
    - Windows : ``` source venv/Scripts/activate  ```
    - Linux : ``` source venv/bin/activate ```
- Installer les bibliothèques nécessaires au projet ``` pip install -r requirements.txt ```
- Allez au centre du projet Django ``` cd apphtmx/ ```
- Lancer l'appli Django ``` python manage.py runserver```


