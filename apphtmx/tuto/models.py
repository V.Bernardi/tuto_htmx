from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.functions import Lower


class User(AbstractUser):
    pass

class Tasks(models.Model):
    description = models.CharField(max_length=128)
    users = models.ManyToManyField(User, related_name='tasks', through='UserTasks')

    class Meta:
        ordering = [Lower('description')]

    def get_users_count(self):
        return self.users.count()
    
    def get_users_names(self):
        return ", ".join([user.username for user in self.users.all()])
    
    def is_user_assigned(self, user):
        return self.users.filter(id=user.id).exists()


class UserTasks(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Tasks, on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField()

    class Meta:
        ordering  = ['order']