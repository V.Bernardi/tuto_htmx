from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User, Tasks

class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        # vérifie que password1 == password2  +  passwd un poil compliqué
        fields = ['username', 'password1', 'password2']