from django.db.models.query import QuerySet
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView, ListView
from django.contrib.auth.views import  LoginView, LogoutView
from .forms import RegisterForm
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.shortcuts import redirect, render
from .models import Tasks, UserTasks

from django.views.decorators.http import require_http_methods
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from .utils import get_max_order, reorder
from django.shortcuts import get_object_or_404

class IndexView(TemplateView):
    template_name = 'index.html'

class Login(LoginView):
    template_name = 'registration/login.html'

    redirect_authenticated_user = True

    def get_success_url(self):
        return reverse_lazy('index')

    def form_invalid(self, form):
        messages.error(self.request, 'Invalid username or password.')
        return self.render_to_response(self.get_context_data(form=form))

class RegisterView(FormView):
    form_class = RegisterForm
    template_name = 'registration/register.html'
    success_url = reverse_lazy('login')

    def form_valid(self, form) -> HttpResponse:
        form.save()
        return super().form_valid(form)
    
class Logout(LogoutView):
    pass


# TASKS

class TaskList(LoginRequiredMixin, ListView):
    login_url = '/login/'
    template_name = 'task/task.html'
    model = Tasks
    context_object_name = 'tasks'

    def get_queryset(self):
        user = self.request.user
        return UserTasks.objects.filter(user=user)
        # pour eviter l'enchainement de requetes 
        # return UserTasks.objects.prefetch_related('task').filter(user=self.user)

@login_required
def add_task(request):
    desc = request.POST.get('taskdescription')
    task = Tasks.objects.create(description=desc)
    print("task avant tout :" , task, " oui ", task.description)
    try:
        print("try")
        tasks = task
        print("try task :", tasks)
        print("try 2")
    except:
        print("except")
        tasks = Tasks.objects.create(description=desc)
        print("except tasks :", tasks)
        print("except 2")
    # request.user.tasks.add(task)

    # tasks = request.user.tasks.all()
    if not UserTasks.objects.filter(user=request.user, task=tasks).exists():
        print("if")
        UserTasks.objects.create(user=request.user, task=tasks, order=get_max_order(request.user))
    print("usertasks: ", UserTasks.objects.all())

    messages.success(request, 'Task added successfully')

    return render(request, 'task/task-list.html', {'tasks':UserTasks.objects.filter(user=request.user)})

@login_required
@require_http_methods(['DELETE'])
def delete_task(request, pk):
    # task = Tasks.objects.filter(pk=pk)
    # task.delete()

    # tasks = request.user.tasks.all()
    UserTasks.objects.get(pk=pk).delete()
    reorder(request.user)
    return render(request, 'task/task-list.html', {'tasks':UserTasks.objects.filter(user=request.user)})


# SEARCH

@login_required
def search_task(request):
    search_text = request.POST.get('search')
    usertasks = request.user.tasks.all()

    # values_list =>  transforme un  'query set'  en liste de valeurs
    if search_text != "":
        results = Tasks.objects.filter(description__icontains=search_text).exclude(
            description__in=usertasks.values_list('description', flat=True)
        )[:10] # on limite à 10 résultat
    else:
        results = []

    # results = Tasks.objects.filter(description="pouet")

    print(results)
    return render(request, 'task/search-results.html', {"results":results})


# Toutes les tasks

class AllTaskList(LoginRequiredMixin, ListView):
    login_url = '/login/'
    template_name = 'task/all-tasks.html'
    model = Tasks
    context_object_name = 'tasks'
    paginate_by = 4

    def get_queryset(self):
        return Tasks.objects.all()


def edit_task(request, task_id):
    task = get_object_or_404(Tasks, pk=task_id)

    if request.method == 'POST':
        task.description = request.POST.get('description')
        task.save()
        return redirect('all-task-list')

    return render(request, 'task/edit-task.html', {'task': task})


def task_users(request, task_id):
    task = Tasks.objects.get(id=task_id)
    return render(request, 'task/users-per-task.html', {'nb_user':task.get_users_count(), 'users':task.get_users_names(), \
                                                        'task': task, 'is_user_assigned': task.is_user_assigned(request.user)}) 

def toggle_task_subscription(request, task_id):
    task = Tasks.objects.get(id=task_id)
    if task.is_user_assigned(request.user):
        UserTasks.objects.filter(task=task, user=request.user).delete()
    else:
        order = UserTasks.objects.filter(task=task).count() + 1
        UserTasks.objects.create(task=task, user=request.user, order=order)
    return HttpResponse()

# HTMX 

def check_username(request):
    username = request.POST.get("username")
    if get_user_model().objects.filter(username=username).exists():
        return HttpResponse("<div id='username-error' style='color:red'> Username already exists. </div>")
    else:
        return HttpResponse("<div id='username-error' style='color:green'> All is fine </div>", status=200)


def clear(request):
    return HttpResponse("")

from django.core.paginator import Paginator
from django.conf import settings

def sort(request):
    res=request.POST.getlist('task_order')
   
    tasks=[]
    updated=[]
    usertasks= UserTasks.objects.prefetch_related('task').filter(user=request.user)
    for count,pk in enumerate(res,start=1):
        usertask=UserTasks.objects.get(pk=pk)
        # usertask.order=count
        # usertask.save()
        # tasks.append(usertask)
        if usertask.order != count:
            usertask.order = count
            updated.append(usertask)
    UserTasks.objects.bulk_update(updated,['order'])
    paginator=Paginator(tasks,settings.PAGINATE_BY)
    page_number= len(res) /settings.PAGINATE_BY
    page_obj= paginator.get_page(page_number)
    context = {'tasks':tasks, 'page_obj':page_obj}

    return render(request,'task/task_list.html',context)
