from django.db.models import Max
from .models import UserTasks

def get_max_order(user):
    usertasks = UserTasks.objects.filter(user=user)
    if not usertasks.exists():
        return 1
    else:
        max = usertasks.aggregate(max_order=Max('order'))['max_order']
        return max + 1
    
def reorder(user):
    usertasks = UserTasks.objects.filter(user=user)
    if not usertasks.exists():
        return None

    nbtasks = usertasks.count()
    new_ordering = range(1, nbtasks+1)
    for order, usertasks in zip(new_ordering, usertasks):
        usertasks.order = order
        usertasks.save()