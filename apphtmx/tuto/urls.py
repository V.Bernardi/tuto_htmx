from django.urls import path
from tuto import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),    
    path('login/', views.Login.as_view(), name='login'),    
    path('logout/', views.Logout.as_view(), name='logout'),    
    path('register/', views.RegisterView.as_view(), name='register'),    
    path('tasks/', views.TaskList.as_view(), name='task-list'),    
    path('all_tasks/', views.AllTaskList.as_view(), name='all-task-list'), 
]

htmx_views = [
    path('check_username', views.check_username, name='check_username'),
    path('add_task', views.add_task, name='add_task'),
    path('delete_task/<int:pk>', views.delete_task, name='delete_task'),
    path('search_task/', views.search_task, name='search_task'),
    path('sort/', views.sort, name='sort'),
    path('clear/', views.clear, name='clear'),
    path('edit_task/<int:task_id>/', views.edit_task, name='edit-task'),
    path('tasks/<int:task_id>/users/', views.task_users, name='task-users'),
    path('tasks/<int:task_id>/toggle-subscription/', views.toggle_task_subscription, name='toggle_task_subscription'),
]

urlpatterns += htmx_views